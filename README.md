# The Pyramid API cookiecutter template

A cookiecutter template to use when creating a Pyramid-based Python API.

## TL;DR

To generate an API, make sure you have cookiecutter installed (on MacOS):

```brew install cookiecutter```

Then navigate to your (new) repo and reference the cookiecutter template:

```bash

cd ~/path/to/new/repo
cookiecutter ~/path/to/pyramid-api-cookiecutter

```

Finally, simply answer the questions asked by cookiecutter and you're set.

To run the server, cd into the new repo you just created and fire it up with pserve:

```pserve ./new_repo/app_settings/development.ini --reload```
