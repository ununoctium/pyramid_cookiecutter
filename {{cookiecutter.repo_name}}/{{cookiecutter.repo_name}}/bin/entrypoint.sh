#!/bin/bash

exec gunicorn --bind 0.0.0.0:8000 \
--paste {{ cookiecutter.repo_name }}/app_settings/production.ini \
--name {{ cookiecutter.repo_name }} \
--workers 4 \
--log-level=info \
--log-file=/var/log/gunicorn.log \
--access-logfile=/var/log/gunicorn-access.log
